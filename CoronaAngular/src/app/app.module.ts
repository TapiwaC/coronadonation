import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import {SelectRequiredValidatorDirective} from './shared/select-required-validator.directive';
import { NgxPayPalModule } from 'ngx-paypal';

import { AppRoutingModule, routingList } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './home/homepage.component';
import { PageNotFoundComponent } from './others/page-not-found.component';
import { TestchartComponent } from './charts/testchart.component';
import { TestService } from './charts/test.service';
import { CreateDonationComponent } from './donation/create-donation.component';

import {DonationService} from './donation/donation.service';
import { AddDonationComponent } from './donation/add-donation.component';
import { SignUpComponent } from './user/sign-up.component';
import { AboutusComponent } from './about/aboutus.component';
import { SigninComponent } from './user/signin.component';
import { UrlcontrolService } from './service/urlcontrol.service';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    PageNotFoundComponent,
    routingList,
    TestchartComponent,
    CreateDonationComponent,
    SelectRequiredValidatorDirective,
    AddDonationComponent,
    SignUpComponent,
    AboutusComponent,
    SigninComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPayPalModule
  ],
  providers: [TestService,DonationService,UrlcontrolService],
  bootstrap: [AppComponent]
})
export class AppModule { }
