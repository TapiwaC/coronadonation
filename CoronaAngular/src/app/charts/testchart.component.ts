import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { TestService } from './test.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-testchart',
  templateUrl: './testchart.component.html',
  styleUrls: ['./testchart.component.css']
})
export class TestchartComponent implements OnInit,AfterViewInit {
  chart = []; // This will hold our chart info
  chart1 =[];
  chart2 =[];
  chartData = [];
  datasets = [];
  labels = [];
  datasets1 = [];
  labels1 = [];
 
  @ViewChild('canvas') chartElementRef: ElementRef;
 // @ViewChild('canvas1') chartElementRef1: ElementRef;
  @ViewChild('canvas2') chartElementRef2: ElementRef;
  

  constructor(private chartService: TestService) { }

  ngOnInit(): void {
   

  };
ngAfterViewInit() {
  this.chartService.getCategoriesData().subscribe((data) => {
    if(data){
      for(var donation of data){
        //if donation is for ALL groupname, distribute the amounts equally amoung the groups
        if(donation.donationGroupName != "All"){
          this.labels1.push(donation.donationGroupName);
          this.datasets1.push(Number(donation.sum.toFixed(0)));
        }

      }
      this.chart2 = new Chart(this.chartElementRef2.nativeElement.getContext('2d'), {
        type: 'doughnut',
        data: {
          labels: this.labels1,
          datasets: [
            {
              label: "Donations",
              backgroundColor: ["blue", "gray", "green"],
              data: this.datasets1
            }
          ]
        },
        options: {
          title: {
            display: true,
            text: 'Amount Received per Category(ZAR)'
          },
          cutoutPercentage: 40
        },
        
    });
    }
  });

  this.chartService.getTop5DonorData().subscribe((data) => {
    if(data){
      this.chartData = data;
      for(var donor of this.chartData){
        if(donor.anonymous == false){
          this.labels.push(donor.firstName + " " + donor.lastName);
        }
        else{
          this.labels.push(donor.alias);
        }
        this.datasets.push(donor.amount.toFixed(0));
      }

      this.chart = new Chart(this.chartElementRef.nativeElement.getContext('2d'), {
       type: 'horizontalBar',
       fill: false,
       data: {
         labels: this.labels,
         datasets: [
           { 
             data: this.datasets,
             backgroundColor: ["purple", "yellow", "black", "pink", "brown"], 
           },
          
         ]
       },
       options: {
         legend: {
           display: false
         },
         scales: {
           xAxes: [{
             display: true,
             
             scaleLabel: {
              display: true,
              labelString: "Total Donation Amount(ZAR)"
            },
            
           }],
           yAxes: [{
             display: true,
             scaleLabel: {
              display: true,
              labelString: "Donor Name"
            }
             
           }],
         },
         title: {
           display: true,
           text: 'Top 5 donors - Cumulative'
         },
       }
     });  
    }
 },
 (err) => {
   if(err){
     console.log(err);
   }
 }
 );
    
  };

}
