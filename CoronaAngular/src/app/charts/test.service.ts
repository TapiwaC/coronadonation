import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { donor } from '../models/donor.model';

@Injectable({
  providedIn: 'root'
})

export class TestService {

  constructor(private _http: HttpClient) { }
  private topDonorsURL = "https://sheltered-waters-91495.herokuapp.com/payment/donation/top/5";
  private categoriesURL = "https://sheltered-waters-91495.herokuapp.com/payment/donation/groups/4";
  
  getTop5DonorData():Observable<any>{
    let username='frontend';
    let password='angularapp';
    const headers = new HttpHeaders({Authorization:'Basic '+btoa(username+':'+password)});
    return this._http.get<any>(this.topDonorsURL,{headers});
  }

  getCategoriesData():Observable<any>{
    let username='frontend';
    let password='angularapp';
    const headers = new HttpHeaders({Authorization:'Basic '+btoa(username+':'+password)});
    return this._http.get<any>(this.categoriesURL,{headers}); 
  }
}
