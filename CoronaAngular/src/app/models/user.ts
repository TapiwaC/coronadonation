export interface User {
    name?:string;
    surname?:string;
    username?:string;
    email:string;
    phone?:string;
    password:string;
    role?:string;
    active?:boolean;
}
