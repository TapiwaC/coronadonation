export interface IDonation{
    firstName: string;
    lastName: string;
    alias?: string;
    email: string;
    anonymous:boolean;
    comment?: string;
    country: string;
    amount: number;
    donationGroup:number;
    donationGroupName: string;
    date: string;
    status: string;
    phone?: number;
    chkDonationGrp?: boolean
}