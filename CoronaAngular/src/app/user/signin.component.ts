import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private authentication:AuthenticationService,private route: Router) { }
  user: User;

  ngOnInit(): void {
    this.user = {
      email:"",
      password: ""
    };
  }
  
  onSignIn(values){
    if(this.authentication.authenticate(values.email, values.password)){
      this.route.navigate(['/home']);
    }
    
  }

}
