import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class SignupService {
  private createURL = "https://sheltered-waters-91495.herokuapp.com/payment/user/create";

  constructor(private http: HttpClient) { }
  createUser(user:User){
    let dataURL = "name="+user.name+"&surname=" + user.surname +
    "&password=" + user.password +
    "&userName=" + user.username +
    "&phone=" + user.phone +
    "&email=" + user.email +
    "&role=" + user.role +
    "&active=" + user.active;
    var headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    console.log(dataURL);
    return this.http.post(this.createURL,dataURL,{headers});
  }
}
