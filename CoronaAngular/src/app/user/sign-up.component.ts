import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { SignupService } from './signup.service';
import { IRole } from '../models/role.model';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  user: User;
  roles: IRole[];
  constructor(private signupService: SignupService) { }

  ngOnInit(): void {
    this.roles = [
      {Id:0,Name:"Admin"},
      {Id: 1, Name: "User"}
    ]
    this.user = {
      email: "",
      password: "",
      username: "",
      active: false,
      name: "",
      phone: "",
      role:"",
      surname:""

    }
  }
  onSignUp(values){

    values.role = this.roles[Number(values.role)].Name;//get the role name to post
    this.signupService.createUser(values).subscribe(
      (data) => {
        console.log("Successful");
        console.log(data);
      },
      (error) =>{
        console.log("Errors");
      }
    );
  }

}
