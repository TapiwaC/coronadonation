import { Component, OnInit } from '@angular/core';
import { IDonation } from '../models/donation.model';
import { IDonationGroup } from '../models/donationgrp.model';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DonationService } from './donation.service';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import { formatDate } from '@angular/common';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-create-donation',
  templateUrl: './create-donation.component.html',
  styleUrls: ['./create-donation.component.css']
})
export class CreateDonationComponent implements OnInit {
  donation: IDonation;
  groups:IDonationGroup[] = [];
  isAnonymous:boolean = false;
  isValidGroup = false;
  isSubmitted = false;
  public payPalConfig?: IPayPalConfig;
  paypalDonationData: IDonation;
  paypalDonation: IDonation;
  isDonationGrp = false;
  isPaymentSuccessful = false;
  
  constructor(private router: Router, private donationService: DonationService,private authService:AuthenticationService) { }

  ngOnInit(): void {
    this.initConfig();
    this.donation = {
      amount: null,
      anonymous:false,
      comment:"",
      country:"",
      email:"",
      firstName:"",
      lastName:"",
      donationGroup: -1,
      donationGroupName: "",
      date:"",
      status: "",
      alias: "",
      phone:null,
      chkDonationGrp: false

    };
    this.groups = [
      {Id: 0, Name: "Medication"},
      {Id: 1, Name: "Ventilators"},
      {Id: 2, Name: "Food"},
      {Id: 3, Name: "All"}
    ];
  }

  saveDonation(values:IDonation){
    /************* FORMATING THE OBJECT RETURNED FROM THE FORM *****************/
  this.authService.authenticate('frontend','angularapp');
    //convert to US dollars for paypal, later implement using API
    
    values.amount = Number(values.amount) * this.donationService.exchange_rate;


    //If no donation group is set, default group to 'ALL' so that its a donation for all groups
    if(values.donationGroup == null){
      values.donationGroup = 3;
    }

    //set group name
    for(var grp of this.groups){
      if(grp.Id == values.donationGroup){
        values.donationGroupName = grp.Name;
      }
    }
    
    /*
    //set current datetime
    values.date = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', 'en-US');

    this.donationService.addDonation(values).subscribe(
      (data) => {

        console.log("Successful");
        this.isPaymentSuccessful = true;
        //this.router.navigateByUrl('/home');
      }
    ); 
    */
    this.paypalDonationData = values;
    this.isSubmitted = true;    
  }
  onChangechkAnonymous(value){
    if(value == true){
      this.isAnonymous = true;
    }
    else{
      this.isAnonymous = false;
    }
  }
  onChangechkDonationGrp(value){
    if(value == true){
      this.isDonationGrp = true;
    }
    else{
      this.isDonationGrp = false;
    }

  }

  onChangeGroup(selected: string){
    if(selected === "-1"){
      this.isValidGroup = false;
    }
    else{
      this.isValidGroup = true;
    }
  }
  private initConfig(): void {
    this.payPalConfig = {
    currency: 'USD',
    clientId: 'AWsaiKg28vvlfr8Cz70oj7HlMTWgwC6bNoZT-LVwwp0EdHsOQLteiPFyv8uv16r0Xd_HNnELQ9Dg_Rcu',
    createOrderOnClient: (data) => <ICreateOrderRequest>{
      intent: 'CAPTURE',
      payer: {
        email_address: this.paypalDonationData.email,
        name:{
          given_name: this.paypalDonationData.firstName,
          surname: this.paypalDonationData.lastName
        }
      },
      purchase_units: [
        {
          amount: {
            currency_code: 'USD',
            value: this.paypalDonationData.amount.toString(),
          },

        }
      ]
    },
    advanced: {
      commit: 'true'
    },
    style: {
      label: 'paypal',
      layout: 'vertical'
    },
    onApprove: (data, actions) => {
      console.log('Transaction was approved, but not authorized yet');
      actions.order.get().then(details => {
        console.log('onApprove - you can get full order details inside onApprove: ');
      });
    },
    onClientAuthorization: (data) => {
      if(data){
        console.log('Transaction successfull');
      //this.showSuccess = true;

      for(var grp of this.groups){
        if(grp.Id == this.paypalDonationData.donationGroup){
          this.paypalDonationData.donationGroupName = grp.Name;
        }
      }

      //post data to server***modify data to be that sent from paypal
      this.paypalDonation = {
        amount: Number(data.purchase_units[0].amount.value),
        country: data.payer.address.country_code,
        email:data.payer.email_address,
        alias: this.paypalDonationData.alias,
        lastName: data.payer.name.surname,
        firstName: data.payer.name.given_name,
        anonymous: this.isAnonymous,
        date: data.create_time,
        donationGroup: this.paypalDonationData.donationGroup,
        donationGroupName: this.paypalDonationData.donationGroupName,
        status: data.status,
        comment: this.paypalDonationData.comment,
      }
      this.donationService.addDonation(this.paypalDonation).subscribe(
        (data) => {
          if(data){
            console.log(data);
            this.isPaymentSuccessful = true;
            //this.router.navigateByUrl('/home');
          }
        },
        (error) => {
          console.log(error);
        }
      )
      }
      else{
        console.log("Payment was not successful")
      }
      
    },
    onCancel: (data, actions) => {
      console.log('transaction cancelled');
    },
    onError: err => {
      console.log('Error');
    },
    onClick: (data, actions) => {
      console.log("You will be redirected to the paypal payment page, please be patient");
    },
  };
  }
}
