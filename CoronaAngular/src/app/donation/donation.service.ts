import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { IDonation } from '../models/donation.model';

@Injectable({
  providedIn: 'root'
})
export class DonationService {

  constructor(private http: HttpClient) { }
  public sharedDonation:IDonation;
  public exchange_rate: number = 0.055;

  credentials={password:'angularapp',username:'frontend'};

  addDonation(donation:IDonation){
    var url: string="https://sheltered-waters-91495.herokuapp.com/payment/donation/save";
    let username='frontend';
    let password='angularapp';
    const headers = new HttpHeaders({Authorization:'Basic '+btoa(username+':'+password) });
    
    // var url1: string="http://localhost:8083/payment/donation/save"; 
    // var url2: string="http://localhost:8083/payment/donation/all";
    // var result = this.http.get(url2,{headers}).subscribe()
    


    
    let data:IDonation={
      amount: donation.amount,
      donationGroup: donation.donationGroup,
      donationGroupName:donation.donationGroupName,
      firstName: donation.firstName,
      lastName: donation.lastName,
      anonymous: donation.anonymous,
      comment: donation.comment,
      country: donation.country,
      email: donation.email,
      date:donation.date,
      status: donation.status,
      alias: donation.alias
    };
    //Convert amount to rands before saving
    data.amount = data.amount/this.exchange_rate;
    //Email is a unique ID hence if email is not set give a random email based on the current datetime
    if(data.email == "" || !data.email){
      data.email = data.date.toString() + "@randomemail";
    }

    let dataURL = "amount="+data.amount+"&donationGroup=" + data.donationGroup +
    "&donationGroupName=" + data.donationGroupName +
    "&firstName=" + data.firstName +
    "&lastName=" + data.lastName +
    "&alias=" + data.alias +
    "&anonymous=" + data.anonymous +
    "&comment=" + data.comment +
    "&country=" + data.country +
    "&email=" + data.email +
    "&date=" +  data.date+
    "&status=" + data.status;
    console.log(data)
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    //paypal data
    console.log(dataURL);
    return this.http.post(url,data,{headers});
  }
}
