import { Component, OnInit } from '@angular/core';
import { IDonation } from '../models/donation.model';
import { IDonationGroup } from '../models/donationgrp.model';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DonationService } from './donation.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-add-donation',
  templateUrl: './add-donation.component.html',
  styleUrls: ['./add-donation.component.css']
})
export class AddDonationComponent implements OnInit {
  donation: IDonation;
  groups:IDonationGroup[] = [];
  isAnonymous:boolean = false;
  isValidGroup = false;
  isSubmitted = false;
  isDonationGrp = false;
  isPaymentSuccessful = false;
  constructor(private router: Router, private donationService: DonationService) { }

  ngOnInit(): void {
    this.donation = {
      amount: null,
      anonymous:false,
      comment:"",
      country:"",
      email:"",
      firstName:"",
      lastName:"",
      donationGroup: -1,
      donationGroupName: "",
      date:"",
      status: "",
      alias: "",
      phone:null,
      chkDonationGrp: false

    };
    this.groups = [
      {Id: 0, Name: "Medication"},
      {Id: 1, Name: "Ventilators"},
      {Id: 2, Name: "Food"},
      {Id: 3, Name: "All"}
    ];
  }

  saveDonation(values:IDonation){
    /************* FORMATING THE OBJECT RETURNED FROM THE FORM *****************/
  
    //convert to US dollars for paypal, later implement using API
    values.amount = Number(values.amount) * this.donationService.exchange_rate;


    //If no donation group is set, default group to 'ALL' so that its a donation for all groups
    if(values.donationGroup == null){
      values.donationGroup = 3;
    }

    //set group name
    for(var grp of this.groups){
      if(grp.Id == values.donationGroup){
        values.donationGroupName = grp.Name;
      }
    }
    
    
    //set current datetime
    values.date = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', 'en-US');

    this.donationService.addDonation(values).subscribe(
      (data) => {

        console.log("Successful");
        this.isPaymentSuccessful = true;
        this.isSubmitted = true; 
      }
    ); 
    
       
  }

  onChangechkAnonymous(value){
    if(value == true){
      this.isAnonymous = true;
    }
    else{
      this.isAnonymous = false;
    }
  }
  onChangechkDonationGrp(value){
    if(value == true){
      this.isDonationGrp = true;
    }
    else{
      this.isDonationGrp = false;
    }

  }

  onChangeGroup(selected: string){
    if(selected === "-1"){
      this.isValidGroup = false;
    }
    else{
      this.isValidGroup = true;
    }
  }

}
