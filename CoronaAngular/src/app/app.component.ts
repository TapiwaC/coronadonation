import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {UrlcontrolService} from '../app/service/urlcontrol.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router, private urlcontrol: UrlcontrolService){}
  title = 'Zimbabwe Covid-19 Relief Fund';
  onClickDonate(){
    this.router.navigate(['donation']);
  }
  canActivate(){
    return this.urlcontrol.canActivate();
  }
}
