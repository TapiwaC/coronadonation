import { Component, OnInit, HostListener } from '@angular/core';
import { donor } from '../models/donor.model';
import { TestService } from '../charts/test.service';
import { HomeService } from './home.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  width: number;
  name: string;
  progress: number;
  goal: number= 250000;

  width1: number;
  name1: string;
  progress1: number;
  goal1: number = 250000;

  width2: number;
  name2: string;
  progress2: number;
  goal2: number = 250000;

  topDonorName: string;
  topDonorTotal: number;

  latestDonors: donor[] = [];
  totalDonationAmount:number =0;
 

  constructor(private chartService: TestService, private homeService: HomeService) { }

  ngOnInit(): void {
    this.chartService.getTop5DonorData().subscribe((data) => {
      if(data){
        if(data[0].anonymous == false ){
          this.topDonorName = data[0].firstName + " " + data[0].lastName;
        }
        else{
          this.topDonorName = data[0].alias;
        }
        this.topDonorTotal = data[0].amount;
      }
    });

    this.chartService.getCategoriesData().subscribe((data) => {
      if(data){
        for(var category of data){
          this.totalDonationAmount = this.totalDonationAmount + category.sum;
          if(category.donationGroupName == "Food")
        {
          this.width1 = (category.sum.toFixed(2)/this.goal1) * 100;
          this.name1 = "ZAR" + this.goal1.toString() + " goal";
          this.progress1 = this.width1;
          if(this.width1 >100){
            this.width1 = 100;
          }
        }
        else if(category.donationGroupName == "Medication")
        {
          this.name2 = "ZAR" + this.goal2.toString() + " goal";
          this.width2 = (category.sum.toFixed(2)/this.goal2)*100;
          this.progress2 = this.width2;
          if(this.width2 >100){
            this.width2 = 100;
          }
        }
        else if(category.donationGroupName == "Ventilators")
        {
          this.width = (category.sum.toFixed(2)/this.goal) * 100;
          this.name = "ZAR" + this.goal.toString() + " goal";
          this.progress = this.width;
          if(this.width >100){
            this.width = 100;
          }
        }
        }
       
      }

    });

    this.homeService.getLatestDonations().subscribe((data) =>{
      if(data){
        for(let donor of data){
          //Ensure donor has a name if anonymous 
          if(donor.anonymous == true){
            donor.firstName = donor.alias;
            donor.lastName = "";
          }
        }
        this.latestDonors = data;
      }
    });
  }
}
