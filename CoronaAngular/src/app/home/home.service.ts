import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http:HttpClient) { }
  private latestdonationURL = "https://sheltered-waters-91495.herokuapp.com/payment/donation/recent/10";
  getLatestDonations():Observable<any>{
    let username='frontend';
    let password='angularapp';
    const headers = new HttpHeaders({Authorization:'Basic '+btoa(username+':'+password) });
    return this.http.get<any>(this.latestdonationURL,{headers});
  }
}
