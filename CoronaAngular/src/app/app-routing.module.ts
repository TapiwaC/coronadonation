import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './home/homepage.component';
import { PageNotFoundComponent } from './others/page-not-found.component';
import { TestchartComponent } from './charts/testchart.component';
import { CreateDonationComponent } from './donation/create-donation.component';
import { AddDonationComponent } from './donation/add-donation.component';
import { SignUpComponent } from './user/sign-up.component';
import { AboutusComponent } from './about/aboutus.component';
import { SigninComponent } from './user/signin.component';
import { UrlcontrolService } from './service/urlcontrol.service';


const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'zimcovid19fund', component: HomepageComponent},
  {path: 'charts/donation', component: TestchartComponent},
  {path: 'donation', component: CreateDonationComponent},
  {path: 'manualdonation', component: AddDonationComponent,canActivate: [UrlcontrolService]},
  {path: 'zimcovid19fund/donation', component: CreateDonationComponent},
  {path: 'user/signin', component: SigninComponent},
  {path: 'about', component: AboutusComponent},
  {path: 'user/signup', component: SignUpComponent},
  {path:'**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingList = [HomepageComponent,PageNotFoundComponent, TestchartComponent, 
  CreateDonationComponent,AddDonationComponent,SignUpComponent,AboutusComponent,SigninComponent];
