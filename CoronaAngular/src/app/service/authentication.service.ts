import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  

  authenticate(username,password){
    var url: string="https://sheltered-waters-91495.herokuapp.com/payment/user/authenticate";
    let username1='frontend';
    let password1='angularapp';
    //let user:User;
    const headers = new HttpHeaders({Authorization:'Basic '+btoa(username+':'+password) });
    let data = {'username':username, 'password':password};
    let user=this.http.get<User>(url,{headers}).subscribe(
      (data) =>{
        console.log(data);
        console.log("success");
      },
      (error) =>{
        console.log("dsfjsdkfndskanfds");
        sessionStorage.removeItem('username');
      }
          )   

    if(user==null)
    {
      console.log("User failed: " + user);
      sessionStorage.removeItem('username');
    return false;
  }else{console.log("User is successful: " + user);
    
  sessionStorage.setItem('username', username);
  return true;}
    
  }

  isUserLoggedIn(){
    let user = sessionStorage.getItem('username');
    console.log("User is :"+user);
    
    return !(user==null);
  }

  logOut(){
    sessionStorage.removeItem('username');
  }
}
