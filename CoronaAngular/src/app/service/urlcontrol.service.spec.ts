import { TestBed } from '@angular/core/testing';

import { UrlcontrolService } from './urlcontrol.service';

describe('UrlcontrolService', () => {
  let service: UrlcontrolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UrlcontrolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
