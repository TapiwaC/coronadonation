import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UrlcontrolService implements CanActivate {

  constructor(private authService: AuthenticationService,private routes:Router) { }

  canActivate(){
    if(this.authService.isUserLoggedIn()){
      return true;
    }

    this.routes.navigate['/user/signup']
    return false;
  }
}

